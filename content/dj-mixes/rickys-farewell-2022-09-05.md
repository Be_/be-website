+++
title = "Ricky's Farewell Party 2022-09-05"
date = 2022-07-26

[extra]
file = "https://pub-6a26b65f31aa4f40b5c418ef13aac133.r2.dev/Be%20-%20Ricky's%20Farewell%20Party%202022-09-05.flac"
file_size = "644 MB"
+++

I played this set at a farewell party at a friend's house.

Playlist:

```
FILE "Be - Ricky's Farewell Party 2022-09-05.flac" WAVE
  TRACK 01 AUDIO
    TITLE "Umakhulu"
    PERFORMER "Ayanda Sikade"
    INDEX 01 00:01:60
  TRACK 02 AUDIO
    TITLE "Bambro Koyo Ganda (Feat. Innov Gnawa)"
    PERFORMER "Bonobo"
    INDEX 01 02:56:21
  TRACK 03 AUDIO
    TITLE "Umakhulu"
    PERFORMER "Ayanda Sikade"
    INDEX 01 07:02:45
  TRACK 04 AUDIO
    TITLE "Little Oranges"
    PERFORMER "B Complex"
    INDEX 01 08:55:37
  TRACK 05 AUDIO
    TITLE "AibreánAcid"
    PERFORMER "Boxcutter"
    INDEX 01 13:42:65
  TRACK 06 AUDIO
    TITLE "How To Navigate"
    PERFORMER "Little People"
    INDEX 01 15:35:57
  TRACK 07 AUDIO
    TITLE "Café Pilon"
    PERFORMER "Jane Bunnett and Maqueque"
    INDEX 01 20:43:49
  TRACK 08 AUDIO
    TITLE "Get Your Snack On"
    PERFORMER "Amon Tobin"
    INDEX 01 25:10:37
  TRACK 09 AUDIO
    TITLE "Dat Mundo"
    PERFORMER "Erothyme"
    INDEX 01 29:16:61
  TRACK 10 AUDIO
    TITLE "Lowrider"
    PERFORMER "Yussef Kamaal"
    INDEX 01 34:55:38
  TRACK 11 AUDIO
    TITLE "Ever Decreasing Circles"
    PERFORMER "Tipper"
    INDEX 01 38:10:38
  TRACK 12 AUDIO
    TITLE "Little Boy"
    PERFORMER "SAULT"
    INDEX 01 42:27:06
  TRACK 13 AUDIO
    TITLE "Getting There"
    PERFORMER "Chris Dacosse"
    INDEX 01 46:02:46
  TRACK 14 AUDIO
    TITLE "Time for Space"
    PERFORMER "Emancipator"
    INDEX 01 49:07:26
  TRACK 15 AUDIO
    TITLE "No Strings"
    PERFORMER "Xeuphoria"
    INDEX 01 56:08:11
  TRACK 16 AUDIO
    TITLE "We Don't Win"
    PERFORMER "Little People"
    INDEX 01 58:21:42
  TRACK 17 AUDIO
    TITLE "Who Loves Me"
    PERFORMER "Pretty Lights"
    INDEX 01 62:07:27
  TRACK 18 AUDIO
    TITLE "Taiga Biome"
    PERFORMER "Plantrae"
    INDEX 01 67:56:23
  TRACK 19 AUDIO
    TITLE "Parallel Planes"
    PERFORMER "Mumukshu"
    INDEX 01 74:36:43
  TRACK 20 AUDIO
    TITLE "Inspector Gadget"
    PERFORMER "Russ Liquid"
    INDEX 01 80:46:03
  TRACK 21 AUDIO
    TITLE "Spring Fever"
    PERFORMER "Saagara"
    INDEX 01 84:31:63
  TRACK 22 AUDIO
    TITLE "Driftwood Palace"
    PERFORMER "Spoonbill"
    INDEX 01 88:38:11
  TRACK 23 AUDIO
    TITLE "7 Transmission 94 (Parts 1 2)"
    PERFORMER "Bonobo"
    INDEX 01 94:16:63
  TRACK 24 AUDIO
    TITLE "Quartet"
    PERFORMER "griff"
    INDEX 01 101:58:51
  TRACK 25 AUDIO
    TITLE "Mulberry Windows (Aligning Minds Remix)"
    PERFORMER "Invisible Allies"
    INDEX 01 106:56:24
  TRACK 26 AUDIO
    TITLE "I'll Fly Away"
    PERFORMER "Rising Appalachia"
    INDEX 01 110:11:24
  TRACK 27 AUDIO
    TITLE "Fuck Shit Stack"
    PERFORMER "Reggie Watts"
    INDEX 01 114:58:52
  TRACK 28 AUDIO
    TITLE "Bananaphone"
    PERFORMER "Raffi"
    INDEX 01 115:08:71
  TRACK 29 AUDIO
    TITLE "Fuck Shit Stack"
    PERFORMER "Reggie Watts"
    INDEX 01 115:29:36
  TRACK 30 AUDIO
    TITLE "Bananaphone"
    PERFORMER "Raffi"
    INDEX 01 115:39:56
  TRACK 31 AUDIO
    TITLE "Fuck Shit Stack"
    PERFORMER "Reggie Watts"
    INDEX 01 116:31:04
  TRACK 32 AUDIO
    TITLE "Bananaphone"
    PERFORMER "Raffi"
    INDEX 01 116:41:24
  TRACK 33 AUDIO
    TITLE "Fuck Shit Stack"
    PERFORMER "Reggie Watts"
    INDEX 01 118:03:32
```
