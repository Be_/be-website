+++
title = "Great Lakes Flow Fest Sunday Sunrise 2019-09-04"
date = 2019-09-04

[extra]
file = "https://pub-6a26b65f31aa4f40b5c418ef13aac133.r2.dev/Be%20-%20Great%20Lakes%20Flow%20Festival%202019%20Sunday%20Sunrise.flac"
file_size = "1.1 GB"
+++

I had the pleasure of closing [Great Lakes Flow Fest](https://www.greatlakesflow.com/) and keeping the fire spinning going past sunrise on the last morning of the festival.

Playlist:
1. Erothyme - Treespeakers (with Madeline Moondrop)
2. Little People - Talk To Me
3. B Complex - Little Oranges
4. KiloWatts - Zwartkop (feat The We're Home Family Drum Brigade)
5. Tor - Drum Therapy
6. Pretty Lights - Speaking of Happiness
7. Ayla Nereo, The Polish Ambassador, Mr. Lif - Lost & Found
8. Skytree - St. Croix Boomsite
9. Random Rab - Repose (feat Peia)
10. Ayla Nereo - Show Yourself
11. Auditive Escape - New Shade
12. Jane Bunnett and Maqueque - Little Feet
13. B Complex - Beautiful Lies VIP
14. Tipper - Dreamsters VIP
15. Reggie Watts - Fuck Shit Stack
16. Pretty Lights - Sunday School
17. Emancipator - The Way
18. Tycho - Sunrise Projector
19. Auditive Escape - Waking Bliss
20. Lapa - Waves
21. Anchorsong - Expo
22. La Luz - My Golden One
23. CloZee - Unexpected
24. Nujabes - Reflection Eternal
25. Chris Dacosse - Forward Momentu
26. Erothyme - Lead Us to the Sound (with Masura Higasa, Michael Svenson, and Emma Staarbird)
27. Skytree - Horizon Rock
28. Haywyre - Duality
29. Auditive Escape - The Grass Trialogue
30. Emancipator - Greenland
31. Wildlight - Move Like The Ocean (Acoustic)
32. Ochre - Raido
33. Janelle Monae - Dance Apocalyptic
34. Bassnectar - Parade into Centuries
35. Yussef Kamaal - Yo Chavez
36. Nahko and Medicine For The People - Dinner Party
37. Michal Menert & The Pretty Fantastics - All You Get
38. Spoonbill - Wonkball
39. Rodrigo y Gabriela con C.U.B.A. - Tamacun
40. Thandi Ntuli - Uz'ubuye
41. Chris Dacosse - Island Shore
42. Estère - On Another's Life/Under Water Whale Knowledge
43. Ana Tijoux - Vengo
44. Bonobo - Nothing Owed (Live)
45. La Luz - The Creature
46. Tycho - Epigram
47. Uyama Hiroto - Taiko
47. Auditive Escape - La Chou Chou
48. Solar Fields - Staring Into The Nothingness
49. Reggie Watts - Fuck Shit Stack
