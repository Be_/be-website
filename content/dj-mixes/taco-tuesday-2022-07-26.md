+++
title = "Taco Tuesday Fire Jam 2022-07-26"
date = 2022-07-26

[extra]
file = "https://pub-6a26b65f31aa4f40b5c418ef13aac133.r2.dev/Be%20-%20Taco%20Tuesday%20Fire%20Jam%202022-07-26.flac"
file_size = "344 MB"
+++

I played this set at our weekly fire flow jam in Chicago.

Playlist:
```
FILE "Be - Taco Tuesday Fire Jam 2022-07-26.flac" WAVE
  TRACK 01 AUDIO
    TITLE "Afterglow (feat. Soundmouse)"
    PERFORMER "Phaeleh"
    INDEX 01 00:02:31
  TRACK 02 AUDIO
    TITLE "Fold And Arrive"
    PERFORMER "Bluetech"
    INDEX 01 04:39:39
  TRACK 03 AUDIO
    TITLE "chips n dip"
    PERFORMER "moshun"
    INDEX 01 09:57:51
  TRACK 04 AUDIO
    TITLE "Sorus"
    PERFORMER "Tipper"
    INDEX 01 13:33:15
  TRACK 05 AUDIO
    TITLE "Feather"
    PERFORMER "Nujabes"
    INDEX 01 16:27:51
  TRACK 06 AUDIO
    TITLE "Each Other"
    PERFORMER "Random Rab"
    INDEX 01 19:11:67
  TRACK 07 AUDIO
    TITLE "Liveforever (feat Samuel Wexler)"
    PERFORMER "KiloWatts"
    INDEX 01 23:28:35
  TRACK 08 AUDIO
    TITLE "Feeling Better"
    PERFORMER "Michal Menert"
    INDEX 01 27:55:23
  TRACK 09 AUDIO
    TITLE "Dat Mundo"
    PERFORMER "Erothyme"
    INDEX 01 32:52:71
  TRACK 10 AUDIO
    TITLE "Closer"
    PERFORMER "Bonobo"
    INDEX 01 38:41:67
  TRACK 11 AUDIO
    TITLE "Chekere Son"
    PERFORMER "Gilles Peterson's Havana Cultura Band feat. Mayra Caridad Valdés"
    INDEX 01 43:29:20
  TRACK 12 AUDIO
    TITLE "Baralku"
    PERFORMER "Emancipator"
    INDEX 01 48:37:12
  TRACK 13 AUDIO
    TITLE "Veil Lifter"
    PERFORMER "Tipper"
    INDEX 01 51:41:68
  TRACK 14 AUDIO
    TITLE "Downtown"
    PERFORMER "Rising Appalachia and The Human Experience"
    INDEX 01 55:48:16
  TRACK 15 AUDIO
    TITLE "Tonight"
    PERFORMER "Little People"
    INDEX 01 59:34:01
```
